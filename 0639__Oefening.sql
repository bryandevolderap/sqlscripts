insert into Personen (
   Voornaam, Familienaam
)
values (
   'Jean-Paul', 'Sartre'
);

use ModernWays;
insert into Boeken (
   Titel,
   Stad,
   Uitgeverij,
   Verschijningsdatum,
   Commentaar,
   Categorie,
   Personen_Id
)
values (
   'De Woorden',
   '',
   'De Bezige Bij',
   '1961',
   '',
   '',
   (select Id from Personen where
       Familienaam = 'Sartre' and Voornaam = 'Jean-Paul'))