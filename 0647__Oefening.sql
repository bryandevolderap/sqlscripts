USE `aptunes`;
DROP procedure IF EXISTS `MockAlbumReleasesLoop`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `MockAlbumReleasesLoop` (IN extraReleases INT)
BEGIN
DECLARE Counter INT Default 0;
DECLARE success bool;
	loop_loop : LOOP
	
		CALL MockAlbumReleaseWithSuccess(success);
		IF success = 1 then
			SET Counter = Counter + 1;
		END IF;
        IF Counter >= extraReleases THEN
			LEAVE loop_loop;
		END IF;    
        
    END LOOP;
END$$

DELIMITER ;