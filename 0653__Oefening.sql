use aptunes;

delimiter $$
create procedure GetAlbumDuration2(IN album int,out totalDuration smallint unsigned)
sql security invoker
begin
	DECLARE SongDuration tinyint unsigned default 0;
    DECLARE ok bool default false;
	DECLARE songDurationCursor cursor for select Lengte from liedjes where Albums_Id = album; 
	DECLARE continue handler for not found set ok = True;
     
    SET totalDuration = 0;
    OPEN songDurationCursor;
		fetchloop: loop
			fetch songDurationCursor into songDuration;
				IF ok = True THEN 
					leave fetchloop;
				END IF;
            set totalDuration = totalDuration + songDuration;
		end loop;
	CLOSE songDurationCursor;
end $$
delimiter ; 