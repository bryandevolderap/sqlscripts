USE aptunes;
DROP procedure IF EXISTS `CreateAndReleaseAlbum`;

DELIMITER $$

CREATE PROCEDURE CreateAndReleaseAlbum (IN newTitle VARCHAR(100), IN bands_ID INT)
BEGIN

-- door 2 inserst
START TRANSACTION;
INSERT INTO Albums(Titel) VALUES(newTitle);
INSERT INTO AlbumReleases (Bands_Id, Albums_ID) VALUES (bands_ID, last_insert_id());
COMMIT;

END$$

DELIMITER ;