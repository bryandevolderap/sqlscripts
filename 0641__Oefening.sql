USE `aptunes`;
DROP procedure IF EXISTS `aptunes`.`NumerOfGenres`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `NumberOfGenres`(OUT total TINYINT)
BEGIN
SELECT COUNT(*)
INTO total
FROM Genres;
END$$

DELIMITER ;
;
