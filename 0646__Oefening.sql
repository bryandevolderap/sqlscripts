USE aptunes;
DROP procedure IF EXISTS MockAlbumRelease;

DELIMITER $$

CREATE  PROCEDURE MockAlbumRelease(IN extraReleases INT)
BEGIN
DECLARE Counter INT Default 0;
DECLARE success bool;
	REPEAT 
		CALL MockAlbumReleaseWithSuccess(success);
		IF success = 1 then
			SET Counter = Counter + 1;
		END IF;
        UNTIL Counter >= extraReleases
    END REPEAT;

END$$

DELIMITER ;
