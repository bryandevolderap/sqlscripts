USE ModernWays;
SELECT Id FROM Studenten
INNER JOIN Evaluaties 
ON Studenten.Id = Evaluaties.Studenten_ID
GROUP BY Studenten.Id
HAVING AVG(Cijfer) > (SELECT AVG(Cijfer) FROM Evaluaties);