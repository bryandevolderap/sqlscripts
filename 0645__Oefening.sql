USE aptunes;
DROP procedure IF EXISTS MockAlbumReleaseWithSuccess;

DELIMITER $$

CREATE PROCEDURE MockAlbumReleaseWithSuccess(OUT success BOOL)
BEGIN
DECLARE numberOfAlbums INT DEFAULT 0;
DECLARE numberOfBands INT DEFAULT 0;
DECLARE randomAlbumId INT DEFAULT 0;
DECLARE randomBandId INT DEFAULT 0;
	SELECT COUNT(*)
	INTO numberOfAlbums
	FROM Albums;
    
    SELECT COUNT(*)
	INTO numberOfBands
	FROM Bands;
	SET success = FALSE;
	SET randomAlbumId = FLOOR(RAND() * numberOfAlbums) + 1;
    SET randomBandId = FLOOR(RAND() * numberOfBands) + 1;
    
    IF (randomAlbumId,randomBandId) NOT IN (SELECT * FROM Albumreleases)
	THEN 
		INSERT INTO AlbumReleases(Albums_Id,Bands_Id)
		VALUES	(randomAlbumId,randomBandId);
        SET success = TRUE;
        END IF;
END$$

DELIMITER ;