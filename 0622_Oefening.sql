USE Aptunes;
SELECT COUNT(DISTINCT LEFT(Voornaam,9))
FROM Muzikanten;

SELECT COUNT(DISTINCT LEFT(Familienaam,9))
FROM Muzikanten;

CREATE INDEX VoornaamFamilienaamIdx
ON Muzikanten(Voornaam(9), Familienaam(9));
