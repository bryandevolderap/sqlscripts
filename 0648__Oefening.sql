USE `aptunes`;
DROP procedure IF EXISTS `DemonstrateHandlerOrder`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `DemonstrateHandlerOrder` ()
BEGIN
	DECLARE Getal INT DEFAULT 0;
    
    DECLARE EXIT HANDLER FOR SQLSTATE '45002' SELECT 'State 45002 opgevangen. Geen probleem';
    DECLARE EXIT HANDLER FOR SQLEXCEPTION SELECT 'Een algemene fout opgevangen';
    
    SET Getal = FLOOR(RAND()*3)+1;
    IF(Getal = 1) THEN
		SIGNAL SQLSTATE '45001';
	ELSEIF(Getal = 2) THEN
		SIGNAL SQLSTATE '45002';
	ELSEIF(Getal = 3) THEN
		SIGNAL SQLSTATE '45003';
	END IF;
END$$

DELIMITER ;