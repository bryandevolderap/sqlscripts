USE ModernWays;
SET SQL_SAFE_UPDATES = 0;
ALTER TABLE Liedjes ADD COLUMN Genre VARCHAR(20) CHAR SET utf8mb4;
UPDATE Liedjes SET Genre = 'Hard Rock'
WHERE Artiest = 'Van Halen' or
	  Artiest = 'Led Zeppelin';
SET SQL_SAFE_UPDATES = 1;